import org.glassfish.jersey.server.ResourceConfig;

class Resources extends ResourceConfig {

    public Resources() {
        packages("diary.services");
    }
}
