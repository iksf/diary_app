package diary.data;


import com.amazonaws.services.dynamodbv2.datamodeling.*;

@DynamoDBTable(tableName = "Appointments")
public class Appointment {
    private String unique_id;
    private String user_id;
    private String description;
    private long start_date;
    private int duration;

    @DynamoDBHashKey(attributeName = "unique_id")
    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    @DynamoDBIndexHashKey(globalSecondaryIndexName = "user_id_start_date_index", attributeName = "user_id")
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String userId) {
        this.user_id = userId;
    }

    @DynamoDBAttribute(attributeName = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @DynamoDBIndexRangeKey(globalSecondaryIndexName = "user_id_start_date_index", attributeName = "start_date")
    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    @DynamoDBAttribute(attributeName = "duration")
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }


    public Appointment() {
    }

    public Appointment(String uuid, String user, String description, long start, int duration) {
        this.description = description;
        this.user_id = user;
        this.start_date = start;
        this.duration = duration;
        this.unique_id = uuid;
    }


}
