package diary.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import diary.data.Appointment;
import org.apache.commons.lang3.RandomStringUtils;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import static diary.dynamodb.DynamoDBUtil.getDBMapper;

@Path("/add_appointment")
public class UpdateAppointment {
    private void updateAppointmentToDynamoDb(Appointment a) {
        DynamoDBMapper mapper = getDBMapper("eu-west-1", "arn:aws:dynamodb:eu-west-1:529469083613:table/Appointments");
        mapper.save(a);
    }

    @POST
    public Response post(
            @QueryParam("user_id") String user_id,
            @QueryParam("description") String description,
            @QueryParam("start") long start,
            @QueryParam("duration") int duration
    ) {
        try {
            Appointment a = new Appointment(RandomStringUtils.random(10, true, true), user_id, description, start, duration);
            updateAppointmentToDynamoDb(a);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().entity(e.toString()).build();
        }
    }
}
