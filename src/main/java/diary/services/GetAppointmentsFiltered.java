package diary.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.google.gson.Gson;
import diary.data.Appointment;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static diary.dynamodb.DynamoDBUtil.getDBMapper;

@Path("/get_appointments")
public class GetAppointmentsFiltered {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAppointmentsFiltered(
            @QueryParam("user_id") String user_id,
            @QueryParam("start") long start,
            @QueryParam("end") long end
    ) {
        DynamoDBMapper mapper = getDBMapper("eu-west-1", "arn:aws:dynamodb:eu-west-1:529469083613:table/Appointments");
        Map<String, AttributeValue> values = new HashMap<String, AttributeValue>();
        values.put(":user", new AttributeValue().withS(user_id));
        values.put(":start", new AttributeValue().withN(String.valueOf(start)));
        values.put(":end", new AttributeValue().withN(String.valueOf(end)));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withIndexName("user_id_start_date_index").withFilterExpression(
                        "user_id = :user and start_date between :start and :end"
                ).withExpressionAttributeValues(values);
        List<Appointment> result = mapper.scan(Appointment.class, scanExpression);
        Gson gson = new Gson();
        return gson.toJson(result);
    }
}
