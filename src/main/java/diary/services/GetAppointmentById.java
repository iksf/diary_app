package diary.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.google.gson.Gson;
import diary.data.Appointment;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static diary.dynamodb.DynamoDBUtil.getDBMapper;

@Path("/get_appointment_by_id")
public class GetAppointmentById {

    @GET
    public Response get(
            @QueryParam("id") String id
    ) {
        DynamoDBMapper mapper = getDBMapper("eu-west-1", "arn:aws:dynamodb:eu-west-1:529469083613:table/Appointments");
        if (id != null) {
            Appointment a = mapper.load(Appointment.class, id);
            if (a != null) {
                return Response.ok().entity(new Gson().toJson(a)).build();
            }
        }
        return Response.serverError().build();
    }
}
