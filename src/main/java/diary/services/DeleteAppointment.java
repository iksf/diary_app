package diary.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import diary.data.Appointment;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import static diary.dynamodb.DynamoDBUtil.getDBMapper;

@Path("/delete_appointment")
public class DeleteAppointment {
    @DELETE
    public Response deleteAppointment(
            @QueryParam("id") String id
    ) {
        DynamoDBMapper mapper = getDBMapper("eu-west-1", "arn:aws:dynamodb:eu-west-1:529469083613:table/Appointments");
        Appointment appointment = mapper.load(Appointment.class, id);
        if (appointment != null) {
            mapper.delete(appointment);
        }
        return Response.ok().build();
    }
}
