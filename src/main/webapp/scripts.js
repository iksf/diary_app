const val = id => $(`#${id}`).val();

const get_appointments = async () => {
    const params = `user_id=${val("user")}&start=${new Date(val("from")).getTime()}&end=${new Date(val("to")).getTime()}`;
    const response = await fetch("./api/get_appointments?" + params,
        {
            method: "GET"
        }
    );
    const json = await response.json();
    $("#resultsDiv").empty();
    json.map(
        x => {
            let s = $(`<p><b>Description:</b> ${x.description} <b>Start time:</b> ${new Date(x.start_date).toLocaleString()} <b>Duration:</b> ${x.duration} <b>(mins)</b></p>`);
            s.click(
                editor_popup(x));
            return s;
        }
    ).forEach(e => $("#resultsDiv").append(e))
};
const add_appointment_save_button_handler = () => {
    (async () => {
        const desc = $("#addAppointmentDescription").val();
        const date = $("#addAppointmentDate").val();
        const start = $("#addAppointmentStart").val();
        const duration = $("#addAppointmentDuration").val();
        if (desc !== "" && date !== "" && start !== "" && duration !== null) {
            await send_appointment(desc, date, start, duration)
                .catch(console.error)
                .then(get_appointments)
                .catch(console.error)
                .then(() => {
                    $("#addDialog").dialog("close")
                })
        }
    })().catch(console.error)
};

const send_appointment = async (description, date, start, duration) => {
    const user_id = $("#user").val();
    const d = new Date(`${date.toString()}T${start.toString()}`).getTime();
    const param_string = `user_id=${user_id}&description=${description}&start=${d}&duration=${duration}`;
    await fetch("./api/add_appointment?" + param_string, {
        method: `POST`,
    }).catch(console.error);
};

const add_appointments_button_handler = () => {
    if ($("#user").val() !== "") {
        $("#addDialog").dialog("open");
    }
};
const editor_popup = x => () => {
    const start = new Date(x.start_date);
    $("#editAppointmentStart").val(start.toJSON().slice(11, 16));
    $("#editAppointmentDate").val(start.toJSON().slice(0, 10));
    $("#editAppointmentDuration").val(x.duration);
    $("#editAppointmentDescription").val(x.description);
    const close_this_window = async () => {
        $("#editDialog").dialog("close")
    };
    const delete_this_appointment = async () =>
        fetch("./api/delete_appointment?" + `id=${x.unique_id}`, {method: "DELETE"})
            .catch(console.error);
    const edit_appointment_button = $("#editAppointmentSaveButton");
    edit_appointment_button.unbind("click");
    edit_appointment_button.click(() => {
        delete_this_appointment()
            .then(
                async () => {
                    const desc = $("#editAppointmentDescription").val();
                    const date = $("#editAppointmentDate").val();
                    const start = $("#editAppointmentStart").val();
                    const duration = $("#editAppointmentDuration").val();
                    if (desc !== "" && date !== "" && start !== "" && duration !== null) {
                        await send_appointment(desc, date, start, duration)
                            .then(close_this_window).then(get_appointments).catch(console.error)
                    }
                }
            )
    });
    const delete_button = $("#editAppointmentDeleteButton");
    delete_button.unbind("click");
    delete_button.click(() => delete_this_appointment().then(close_this_window).then(get_appointments).catch(console.error));
    if ($("user").val() !== "") {
        $("#editDialog").dialog("open");
    }
};

const on_ready = () => {
    $("#addAppointmentButton").click(add_appointments_button_handler);
    $("#appointmentsButton").click(() => get_appointments().catch(console.error));
    $("#addDialog").dialog({autoOpen: false, modal: true, show: "blind", hide: "blind"});
    $("#editDialog").dialog({autoOpen: false, modal: true, show: "blind", hide: "blind"});
    $("#addAppointmentCancelButton").click(() => $("#addDialog").dialog("close"));
    $("#addAppointmentSaveButton").click(add_appointment_save_button_handler);
    $("#editAppointmentCancelButton").click(() => $("#editDialog").dialog("close"))
};


$(document).ready(on_ready);



